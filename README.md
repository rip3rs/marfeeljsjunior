# Thai Blossom Bistro Website.

### Prerequisities

`Vagrant`

### Instalation

$`vagrant up`

$`sudo vim /etc/hosts`

paste `192.168.33.17   marfeeljsjuniortest.dev`

### Running tests

$`vagrant ssh`

It should go straight to the working folder inside of the box
`/var/www/marfeeljsjuniortest.dev`

$`grunt` to go through all of the settings

$`grunt test` for Jasmine and Jshint

### Run the page

`http://marfeeljsjuniortest.dev`
