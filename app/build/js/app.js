(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
/**
 * @param apiUrl
 * @param params
 * @param callback
 */

module.exports = function (apiUrl, params, callback) {

    'use strict';


    var obj = new XMLHttpRequest(),
        url = apiUrl + params;

    obj.overrideMimeType("application/json");
    obj.open('GET', url, true);
    obj.onreadystatechange = function () {

        if (obj.readyState === 4) {
            callback(obj.responseText, obj.status);
        }

    };


    obj.send(null);

};

},{}],2:[function(require,module,exports){
/**
 * Clear elements / reset calls
 */

'use strict';


module.exports = function (el) {
    if (el.children.length > 0) {
		el.innerHTML = "";
	}
};

},{}],3:[function(require,module,exports){
/**
 * Debounce function to optimize the amount of calls to the api.
 */

'use strict';

module.exports = function (func, wait, immediate) {
	var timeout;
	return function() {
		var context = this,
			args = arguments;

		var later = function() {
			timeout = null;
			if (!immediate) func.apply(context, args);
		};

		var callNow = immediate && !timeout;

		clearTimeout(timeout);

		timeout = setTimeout(later, wait);

		if (callNow) {
			func.apply(context, args);
		}
	};
};

},{}],4:[function(require,module,exports){
/**
 * Get the event target function.
 */


'use strict';


module.exports = function ( event ) {
	event = event || window.event;
	return event.target || event.srcElement;
};

},{}],5:[function(require,module,exports){
/**
 * Call parameters for object seperation on call.
 */


'use strict';

module.exports = function( params ){
  return "?" + Object
        .keys(params)
        .map(function(key){ return key+"="+params[key]; })
        .join("&");
};

},{}],6:[function(require,module,exports){
/**
 *
 * Search Users suggestions.
 *
 */

'use strict';


var AjaxCall = require('../lib/ajaxCall'),
	Debounce = require('../lib/debounce'),
	Parameters = require('../lib/parameters'),
	clearElements = require('../lib/clearElements'),
	eventTarget = require('../lib/eventTarget'),
	$searchInputField = document.getElementById('search--user'),
	$suggestionsContainer = document.getElementById('suggestions--container'),
    $suggestions = document.getElementById('suggestions'),
	$searchBtn = document.getElementById('search--btn');


var removeSearchSuggestion = function () {
	$suggestionsContainer.style.display = "none";
};


var showSearchSuggestion = function () {
	$suggestionsContainer.style.display = "block";
};


var searchSuggestion = Debounce(function () {
	var limitAmountOfSearchUsers = 5,
		amountOfUsers = 1,
		getParams = '',
		params = {
			q: ''
		};

	//add values from input into object.
	params.q = $searchInputField.value;

	clearElements( $suggestions );

	//Wait until 3 letters are written before making a request.
	if ( params.q.length >= 3 && $suggestions.children.length === 0 ) {
		getParams = Parameters(params);

		AjaxCall('https://api.github.com/search/users', getParams, function( response, status ) {
			var getResponse = JSON.parse( response );

			if ( status !== 404 ) {
				if (getResponse.items.length === 0) {
					removeSearchSuggestion();
				} else {
					for ( var users in getResponse.items ) {
						if (getResponse.items.hasOwnProperty( users )) {
							$suggestions.innerHTML += '<li >' + getResponse.items[users].login + '</li>';

							amountOfUsers++;
						}

						if ( amountOfUsers >= limitAmountOfSearchUsers ) {
							break;
						}
					}

					showSearchSuggestion();
				}
			} else {
				$suggestions.innerHTML += '<li class="warning">There was a problem with your request!</li>';
				showSearchSuggestion();
			}
		});

	}
}, 500);


$suggestions.addEventListener( 'click', function ( event ) {
	var value = eventTarget( event );
	$searchInputField.value = value.innerText || value.textContent;
	$searchBtn.click();
	$searchInputField.blur();
});


$searchInputField.addEventListener( 'keyup', function ( event ) {
	if ( event.keyCode === 13 ) {
		$searchBtn.click();
		$searchInputField.blur();
	} else {
		searchSuggestion();
	}
});

},{"../lib/ajaxCall":1,"../lib/clearElements":2,"../lib/debounce":3,"../lib/eventTarget":4,"../lib/parameters":5}],7:[function(require,module,exports){
/**
 *
 * Search Users info.
 *
 */

'use strict';


var AjaxCall = require('../lib/ajaxCall'),
	clearElements = require('../lib/clearElements'),
	$suggestionsContainer = document.getElementById('suggestions--container'),
	$searchBtn = document.getElementById('search--btn');


var removeSearchSuggestion = function () {
	$suggestionsContainer.style.display = "none";
};


var getUserAvatar = function ( response ) {
	return '<div id="user--avatar">' + '<img src="' + response.avatar_url + '"' + 'title=' + response.login + '/></div>';
};


var getUserName = function ( response ) {
	return '<h5 id="user--name">' + '<a href="' + response.url + '">' + '@' + response.login + '</a></h5>';
};


var getFullName = function ( response ) {
	return '<h2 id="full--name">' + response.name + '</h2>';
};


var getUserBio = function ( response ) {
	var userBio;

	if (response.bio === null) {
		userBio = 'No Bio Available';
	}
	else {
		userBio = response.bio;
	}

	return '<h5 id="user--bio">' + userBio + '</h5>';
};


var showUserInfo = function () {
	var $userInfo = document.getElementById('user--info'),
		$searchInputField = document.getElementById('search--user'),
		user = $searchInputField.value;


	removeSearchSuggestion();
	clearElements( $userInfo );

	AjaxCall( 'https://api.github.com/users/', user, function( response, status ) {
		var getResponse = JSON.parse( response );

		if ( status !== 404 ) {
			$userInfo.innerHTML += getUserAvatar(getResponse);
			$userInfo.innerHTML += '<div class="details">' +
			 						getUserName(getResponse) +
			 						getFullName(getResponse) +
			 						getUserBio(getResponse) +
									'</div>';
		} else {
			$userInfo.innerHTML += '<div class="warning">Does Not exist!</div>';
		}

	});
};


$searchBtn.addEventListener('click', showUserInfo);

},{"../lib/ajaxCall":1,"../lib/clearElements":2}],8:[function(require,module,exports){
/**
 *  User repositories
 */

'use strict';


var AjaxCall = require('../lib/ajaxCall'),
	clearElements = require('../lib/clearElements'),
	$searchBtn = document.getElementById('search--btn');


var showUserRepos = function () {
    var $userRepos = document.getElementById('user--repos'),
        $repos = document.getElementById('repos'),
		$searchInputField = document.getElementById('search--user'),
		user = $searchInputField.value;

	clearElements( $repos );

	AjaxCall('https://api.github.com/users/', user + '/repos', function(response, status) {
		var getResponse = JSON.parse(response),
			repos;

		console.log(getResponse);

		if ( status !== 404 ) {
			for ( repos in getResponse ) {
				if (getResponse.hasOwnProperty(repos)) {
					$repos.innerHTML +=
						'<li>' +
							'<div class="repo--container">' +
								'<span class="repo--name"><a href="' + getResponse[repos].html_url + '">' + getResponse[repos].name + '</a></span>' +
								'<span class="repo--info">' +
									'<span class="star--count">' +
										'<i class="fa fa-star" aria-hidden="true"></i>' + getResponse[repos].stargazers_count + '</span>' +
									'<span class="fork--count"><i class="fa fa-code-fork" aria-hidden="true"></i>' + getResponse[repos].forks_count + '</span>' +
								'</span>' +
							'</div>' +
						'</li>';
				}
			}

			$userRepos.style.display = "block";
		}
		else {
			$userRepos.style.display = "none";
		}

	});
};


$searchBtn.addEventListener('click', showUserRepos);

},{"../lib/ajaxCall":1,"../lib/clearElements":2}],9:[function(require,module,exports){


// 'use strict';
//
//
// describe('test///', function () {
//     it('testin moar..', function () {
//
//         expect(true).toBe(true);
//     });
// });

},{}]},{},[1,2,3,4,5,6,7,8,9])
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJhcHAvc3JjL2xpYi9hamF4Q2FsbC5qcyIsImFwcC9zcmMvbGliL2NsZWFyRWxlbWVudHMuanMiLCJhcHAvc3JjL2xpYi9kZWJvdW5jZS5qcyIsImFwcC9zcmMvbGliL2V2ZW50VGFyZ2V0LmpzIiwiYXBwL3NyYy9saWIvcGFyYW1ldGVycy5qcyIsImFwcC9zcmMvbW9kdWxlcy9zZWFyY2hVc2VyLmpzIiwiYXBwL3NyYy9tb2R1bGVzL3VzZXJJbmZvLmpzIiwiYXBwL3NyYy9tb2R1bGVzL3VzZXJSZXBvLmpzIiwiYXBwL3NyYy90ZXN0cy90ZXN0cy50ZXN0LmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FDQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUM1QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDWkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUM1QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDWkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNiQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQzlGQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDN0VBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ3REQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJmaWxlIjoiZ2VuZXJhdGVkLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXNDb250ZW50IjpbIihmdW5jdGlvbiBlKHQsbixyKXtmdW5jdGlvbiBzKG8sdSl7aWYoIW5bb10pe2lmKCF0W29dKXt2YXIgYT10eXBlb2YgcmVxdWlyZT09XCJmdW5jdGlvblwiJiZyZXF1aXJlO2lmKCF1JiZhKXJldHVybiBhKG8sITApO2lmKGkpcmV0dXJuIGkobywhMCk7dmFyIGY9bmV3IEVycm9yKFwiQ2Fubm90IGZpbmQgbW9kdWxlICdcIitvK1wiJ1wiKTt0aHJvdyBmLmNvZGU9XCJNT0RVTEVfTk9UX0ZPVU5EXCIsZn12YXIgbD1uW29dPXtleHBvcnRzOnt9fTt0W29dWzBdLmNhbGwobC5leHBvcnRzLGZ1bmN0aW9uKGUpe3ZhciBuPXRbb11bMV1bZV07cmV0dXJuIHMobj9uOmUpfSxsLGwuZXhwb3J0cyxlLHQsbixyKX1yZXR1cm4gbltvXS5leHBvcnRzfXZhciBpPXR5cGVvZiByZXF1aXJlPT1cImZ1bmN0aW9uXCImJnJlcXVpcmU7Zm9yKHZhciBvPTA7bzxyLmxlbmd0aDtvKyspcyhyW29dKTtyZXR1cm4gc30pIiwiLyoqXG4gKiBAcGFyYW0gYXBpVXJsXG4gKiBAcGFyYW0gcGFyYW1zXG4gKiBAcGFyYW0gY2FsbGJhY2tcbiAqL1xuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIChhcGlVcmwsIHBhcmFtcywgY2FsbGJhY2spIHtcblxuICAgICd1c2Ugc3RyaWN0JztcblxuXG4gICAgdmFyIG9iaiA9IG5ldyBYTUxIdHRwUmVxdWVzdCgpLFxuICAgICAgICB1cmwgPSBhcGlVcmwgKyBwYXJhbXM7XG5cbiAgICBvYmoub3ZlcnJpZGVNaW1lVHlwZShcImFwcGxpY2F0aW9uL2pzb25cIik7XG4gICAgb2JqLm9wZW4oJ0dFVCcsIHVybCwgdHJ1ZSk7XG4gICAgb2JqLm9ucmVhZHlzdGF0ZWNoYW5nZSA9IGZ1bmN0aW9uICgpIHtcblxuICAgICAgICBpZiAob2JqLnJlYWR5U3RhdGUgPT09IDQpIHtcbiAgICAgICAgICAgIGNhbGxiYWNrKG9iai5yZXNwb25zZVRleHQsIG9iai5zdGF0dXMpO1xuICAgICAgICB9XG5cbiAgICB9O1xuXG5cbiAgICBvYmouc2VuZChudWxsKTtcblxufTtcbiIsIi8qKlxuICogQ2xlYXIgZWxlbWVudHMgLyByZXNldCBjYWxsc1xuICovXG5cbid1c2Ugc3RyaWN0JztcblxuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIChlbCkge1xuICAgIGlmIChlbC5jaGlsZHJlbi5sZW5ndGggPiAwKSB7XG5cdFx0ZWwuaW5uZXJIVE1MID0gXCJcIjtcblx0fVxufTtcbiIsIi8qKlxuICogRGVib3VuY2UgZnVuY3Rpb24gdG8gb3B0aW1pemUgdGhlIGFtb3VudCBvZiBjYWxscyB0byB0aGUgYXBpLlxuICovXG5cbid1c2Ugc3RyaWN0JztcblxubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiAoZnVuYywgd2FpdCwgaW1tZWRpYXRlKSB7XG5cdHZhciB0aW1lb3V0O1xuXHRyZXR1cm4gZnVuY3Rpb24oKSB7XG5cdFx0dmFyIGNvbnRleHQgPSB0aGlzLFxuXHRcdFx0YXJncyA9IGFyZ3VtZW50cztcblxuXHRcdHZhciBsYXRlciA9IGZ1bmN0aW9uKCkge1xuXHRcdFx0dGltZW91dCA9IG51bGw7XG5cdFx0XHRpZiAoIWltbWVkaWF0ZSkgZnVuYy5hcHBseShjb250ZXh0LCBhcmdzKTtcblx0XHR9O1xuXG5cdFx0dmFyIGNhbGxOb3cgPSBpbW1lZGlhdGUgJiYgIXRpbWVvdXQ7XG5cblx0XHRjbGVhclRpbWVvdXQodGltZW91dCk7XG5cblx0XHR0aW1lb3V0ID0gc2V0VGltZW91dChsYXRlciwgd2FpdCk7XG5cblx0XHRpZiAoY2FsbE5vdykge1xuXHRcdFx0ZnVuYy5hcHBseShjb250ZXh0LCBhcmdzKTtcblx0XHR9XG5cdH07XG59O1xuIiwiLyoqXG4gKiBHZXQgdGhlIGV2ZW50IHRhcmdldCBmdW5jdGlvbi5cbiAqL1xuXG5cbid1c2Ugc3RyaWN0JztcblxuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uICggZXZlbnQgKSB7XG5cdGV2ZW50ID0gZXZlbnQgfHwgd2luZG93LmV2ZW50O1xuXHRyZXR1cm4gZXZlbnQudGFyZ2V0IHx8IGV2ZW50LnNyY0VsZW1lbnQ7XG59O1xuIiwiLyoqXG4gKiBDYWxsIHBhcmFtZXRlcnMgZm9yIG9iamVjdCBzZXBlcmF0aW9uIG9uIGNhbGwuXG4gKi9cblxuXG4ndXNlIHN0cmljdCc7XG5cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24oIHBhcmFtcyApe1xuICByZXR1cm4gXCI/XCIgKyBPYmplY3RcbiAgICAgICAgLmtleXMocGFyYW1zKVxuICAgICAgICAubWFwKGZ1bmN0aW9uKGtleSl7IHJldHVybiBrZXkrXCI9XCIrcGFyYW1zW2tleV07IH0pXG4gICAgICAgIC5qb2luKFwiJlwiKTtcbn07XG4iLCIvKipcbiAqXG4gKiBTZWFyY2ggVXNlcnMgc3VnZ2VzdGlvbnMuXG4gKlxuICovXG5cbid1c2Ugc3RyaWN0JztcblxuXG52YXIgQWpheENhbGwgPSByZXF1aXJlKCcuLi9saWIvYWpheENhbGwnKSxcblx0RGVib3VuY2UgPSByZXF1aXJlKCcuLi9saWIvZGVib3VuY2UnKSxcblx0UGFyYW1ldGVycyA9IHJlcXVpcmUoJy4uL2xpYi9wYXJhbWV0ZXJzJyksXG5cdGNsZWFyRWxlbWVudHMgPSByZXF1aXJlKCcuLi9saWIvY2xlYXJFbGVtZW50cycpLFxuXHRldmVudFRhcmdldCA9IHJlcXVpcmUoJy4uL2xpYi9ldmVudFRhcmdldCcpLFxuXHQkc2VhcmNoSW5wdXRGaWVsZCA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdzZWFyY2gtLXVzZXInKSxcblx0JHN1Z2dlc3Rpb25zQ29udGFpbmVyID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ3N1Z2dlc3Rpb25zLS1jb250YWluZXInKSxcbiAgICAkc3VnZ2VzdGlvbnMgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnc3VnZ2VzdGlvbnMnKSxcblx0JHNlYXJjaEJ0biA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdzZWFyY2gtLWJ0bicpO1xuXG5cbnZhciByZW1vdmVTZWFyY2hTdWdnZXN0aW9uID0gZnVuY3Rpb24gKCkge1xuXHQkc3VnZ2VzdGlvbnNDb250YWluZXIuc3R5bGUuZGlzcGxheSA9IFwibm9uZVwiO1xufTtcblxuXG52YXIgc2hvd1NlYXJjaFN1Z2dlc3Rpb24gPSBmdW5jdGlvbiAoKSB7XG5cdCRzdWdnZXN0aW9uc0NvbnRhaW5lci5zdHlsZS5kaXNwbGF5ID0gXCJibG9ja1wiO1xufTtcblxuXG52YXIgc2VhcmNoU3VnZ2VzdGlvbiA9IERlYm91bmNlKGZ1bmN0aW9uICgpIHtcblx0dmFyIGxpbWl0QW1vdW50T2ZTZWFyY2hVc2VycyA9IDUsXG5cdFx0YW1vdW50T2ZVc2VycyA9IDEsXG5cdFx0Z2V0UGFyYW1zID0gJycsXG5cdFx0cGFyYW1zID0ge1xuXHRcdFx0cTogJydcblx0XHR9O1xuXG5cdC8vYWRkIHZhbHVlcyBmcm9tIGlucHV0IGludG8gb2JqZWN0LlxuXHRwYXJhbXMucSA9ICRzZWFyY2hJbnB1dEZpZWxkLnZhbHVlO1xuXG5cdGNsZWFyRWxlbWVudHMoICRzdWdnZXN0aW9ucyApO1xuXG5cdC8vV2FpdCB1bnRpbCAzIGxldHRlcnMgYXJlIHdyaXR0ZW4gYmVmb3JlIG1ha2luZyBhIHJlcXVlc3QuXG5cdGlmICggcGFyYW1zLnEubGVuZ3RoID49IDMgJiYgJHN1Z2dlc3Rpb25zLmNoaWxkcmVuLmxlbmd0aCA9PT0gMCApIHtcblx0XHRnZXRQYXJhbXMgPSBQYXJhbWV0ZXJzKHBhcmFtcyk7XG5cblx0XHRBamF4Q2FsbCgnaHR0cHM6Ly9hcGkuZ2l0aHViLmNvbS9zZWFyY2gvdXNlcnMnLCBnZXRQYXJhbXMsIGZ1bmN0aW9uKCByZXNwb25zZSwgc3RhdHVzICkge1xuXHRcdFx0dmFyIGdldFJlc3BvbnNlID0gSlNPTi5wYXJzZSggcmVzcG9uc2UgKTtcblxuXHRcdFx0aWYgKCBzdGF0dXMgIT09IDQwNCApIHtcblx0XHRcdFx0aWYgKGdldFJlc3BvbnNlLml0ZW1zLmxlbmd0aCA9PT0gMCkge1xuXHRcdFx0XHRcdHJlbW92ZVNlYXJjaFN1Z2dlc3Rpb24oKTtcblx0XHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0XHRmb3IgKCB2YXIgdXNlcnMgaW4gZ2V0UmVzcG9uc2UuaXRlbXMgKSB7XG5cdFx0XHRcdFx0XHRpZiAoZ2V0UmVzcG9uc2UuaXRlbXMuaGFzT3duUHJvcGVydHkoIHVzZXJzICkpIHtcblx0XHRcdFx0XHRcdFx0JHN1Z2dlc3Rpb25zLmlubmVySFRNTCArPSAnPGxpID4nICsgZ2V0UmVzcG9uc2UuaXRlbXNbdXNlcnNdLmxvZ2luICsgJzwvbGk+JztcblxuXHRcdFx0XHRcdFx0XHRhbW91bnRPZlVzZXJzKys7XG5cdFx0XHRcdFx0XHR9XG5cblx0XHRcdFx0XHRcdGlmICggYW1vdW50T2ZVc2VycyA+PSBsaW1pdEFtb3VudE9mU2VhcmNoVXNlcnMgKSB7XG5cdFx0XHRcdFx0XHRcdGJyZWFrO1xuXHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdH1cblxuXHRcdFx0XHRcdHNob3dTZWFyY2hTdWdnZXN0aW9uKCk7XG5cdFx0XHRcdH1cblx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdCRzdWdnZXN0aW9ucy5pbm5lckhUTUwgKz0gJzxsaSBjbGFzcz1cIndhcm5pbmdcIj5UaGVyZSB3YXMgYSBwcm9ibGVtIHdpdGggeW91ciByZXF1ZXN0ITwvbGk+Jztcblx0XHRcdFx0c2hvd1NlYXJjaFN1Z2dlc3Rpb24oKTtcblx0XHRcdH1cblx0XHR9KTtcblxuXHR9XG59LCA1MDApO1xuXG5cbiRzdWdnZXN0aW9ucy5hZGRFdmVudExpc3RlbmVyKCAnY2xpY2snLCBmdW5jdGlvbiAoIGV2ZW50ICkge1xuXHR2YXIgdmFsdWUgPSBldmVudFRhcmdldCggZXZlbnQgKTtcblx0JHNlYXJjaElucHV0RmllbGQudmFsdWUgPSB2YWx1ZS5pbm5lclRleHQgfHwgdmFsdWUudGV4dENvbnRlbnQ7XG5cdCRzZWFyY2hCdG4uY2xpY2soKTtcblx0JHNlYXJjaElucHV0RmllbGQuYmx1cigpO1xufSk7XG5cblxuJHNlYXJjaElucHV0RmllbGQuYWRkRXZlbnRMaXN0ZW5lciggJ2tleXVwJywgZnVuY3Rpb24gKCBldmVudCApIHtcblx0aWYgKCBldmVudC5rZXlDb2RlID09PSAxMyApIHtcblx0XHQkc2VhcmNoQnRuLmNsaWNrKCk7XG5cdFx0JHNlYXJjaElucHV0RmllbGQuYmx1cigpO1xuXHR9IGVsc2Uge1xuXHRcdHNlYXJjaFN1Z2dlc3Rpb24oKTtcblx0fVxufSk7XG4iLCIvKipcbiAqXG4gKiBTZWFyY2ggVXNlcnMgaW5mby5cbiAqXG4gKi9cblxuJ3VzZSBzdHJpY3QnO1xuXG5cbnZhciBBamF4Q2FsbCA9IHJlcXVpcmUoJy4uL2xpYi9hamF4Q2FsbCcpLFxuXHRjbGVhckVsZW1lbnRzID0gcmVxdWlyZSgnLi4vbGliL2NsZWFyRWxlbWVudHMnKSxcblx0JHN1Z2dlc3Rpb25zQ29udGFpbmVyID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ3N1Z2dlc3Rpb25zLS1jb250YWluZXInKSxcblx0JHNlYXJjaEJ0biA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdzZWFyY2gtLWJ0bicpO1xuXG5cbnZhciByZW1vdmVTZWFyY2hTdWdnZXN0aW9uID0gZnVuY3Rpb24gKCkge1xuXHQkc3VnZ2VzdGlvbnNDb250YWluZXIuc3R5bGUuZGlzcGxheSA9IFwibm9uZVwiO1xufTtcblxuXG52YXIgZ2V0VXNlckF2YXRhciA9IGZ1bmN0aW9uICggcmVzcG9uc2UgKSB7XG5cdHJldHVybiAnPGRpdiBpZD1cInVzZXItLWF2YXRhclwiPicgKyAnPGltZyBzcmM9XCInICsgcmVzcG9uc2UuYXZhdGFyX3VybCArICdcIicgKyAndGl0bGU9JyArIHJlc3BvbnNlLmxvZ2luICsgJy8+PC9kaXY+Jztcbn07XG5cblxudmFyIGdldFVzZXJOYW1lID0gZnVuY3Rpb24gKCByZXNwb25zZSApIHtcblx0cmV0dXJuICc8aDUgaWQ9XCJ1c2VyLS1uYW1lXCI+JyArICc8YSBocmVmPVwiJyArIHJlc3BvbnNlLnVybCArICdcIj4nICsgJ0AnICsgcmVzcG9uc2UubG9naW4gKyAnPC9hPjwvaDU+Jztcbn07XG5cblxudmFyIGdldEZ1bGxOYW1lID0gZnVuY3Rpb24gKCByZXNwb25zZSApIHtcblx0cmV0dXJuICc8aDIgaWQ9XCJmdWxsLS1uYW1lXCI+JyArIHJlc3BvbnNlLm5hbWUgKyAnPC9oMj4nO1xufTtcblxuXG52YXIgZ2V0VXNlckJpbyA9IGZ1bmN0aW9uICggcmVzcG9uc2UgKSB7XG5cdHZhciB1c2VyQmlvO1xuXG5cdGlmIChyZXNwb25zZS5iaW8gPT09IG51bGwpIHtcblx0XHR1c2VyQmlvID0gJ05vIEJpbyBBdmFpbGFibGUnO1xuXHR9XG5cdGVsc2Uge1xuXHRcdHVzZXJCaW8gPSByZXNwb25zZS5iaW87XG5cdH1cblxuXHRyZXR1cm4gJzxoNSBpZD1cInVzZXItLWJpb1wiPicgKyB1c2VyQmlvICsgJzwvaDU+Jztcbn07XG5cblxudmFyIHNob3dVc2VySW5mbyA9IGZ1bmN0aW9uICgpIHtcblx0dmFyICR1c2VySW5mbyA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCd1c2VyLS1pbmZvJyksXG5cdFx0JHNlYXJjaElucHV0RmllbGQgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnc2VhcmNoLS11c2VyJyksXG5cdFx0dXNlciA9ICRzZWFyY2hJbnB1dEZpZWxkLnZhbHVlO1xuXG5cblx0cmVtb3ZlU2VhcmNoU3VnZ2VzdGlvbigpO1xuXHRjbGVhckVsZW1lbnRzKCAkdXNlckluZm8gKTtcblxuXHRBamF4Q2FsbCggJ2h0dHBzOi8vYXBpLmdpdGh1Yi5jb20vdXNlcnMvJywgdXNlciwgZnVuY3Rpb24oIHJlc3BvbnNlLCBzdGF0dXMgKSB7XG5cdFx0dmFyIGdldFJlc3BvbnNlID0gSlNPTi5wYXJzZSggcmVzcG9uc2UgKTtcblxuXHRcdGlmICggc3RhdHVzICE9PSA0MDQgKSB7XG5cdFx0XHQkdXNlckluZm8uaW5uZXJIVE1MICs9IGdldFVzZXJBdmF0YXIoZ2V0UmVzcG9uc2UpO1xuXHRcdFx0JHVzZXJJbmZvLmlubmVySFRNTCArPSAnPGRpdiBjbGFzcz1cImRldGFpbHNcIj4nICtcblx0XHRcdCBcdFx0XHRcdFx0XHRnZXRVc2VyTmFtZShnZXRSZXNwb25zZSkgK1xuXHRcdFx0IFx0XHRcdFx0XHRcdGdldEZ1bGxOYW1lKGdldFJlc3BvbnNlKSArXG5cdFx0XHQgXHRcdFx0XHRcdFx0Z2V0VXNlckJpbyhnZXRSZXNwb25zZSkgK1xuXHRcdFx0XHRcdFx0XHRcdFx0JzwvZGl2Pic7XG5cdFx0fSBlbHNlIHtcblx0XHRcdCR1c2VySW5mby5pbm5lckhUTUwgKz0gJzxkaXYgY2xhc3M9XCJ3YXJuaW5nXCI+RG9lcyBOb3QgZXhpc3QhPC9kaXY+Jztcblx0XHR9XG5cblx0fSk7XG59O1xuXG5cbiRzZWFyY2hCdG4uYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCBzaG93VXNlckluZm8pO1xuIiwiLyoqXG4gKiAgVXNlciByZXBvc2l0b3JpZXNcbiAqL1xuXG4ndXNlIHN0cmljdCc7XG5cblxudmFyIEFqYXhDYWxsID0gcmVxdWlyZSgnLi4vbGliL2FqYXhDYWxsJyksXG5cdGNsZWFyRWxlbWVudHMgPSByZXF1aXJlKCcuLi9saWIvY2xlYXJFbGVtZW50cycpLFxuXHQkc2VhcmNoQnRuID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ3NlYXJjaC0tYnRuJyk7XG5cblxudmFyIHNob3dVc2VyUmVwb3MgPSBmdW5jdGlvbiAoKSB7XG4gICAgdmFyICR1c2VyUmVwb3MgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgndXNlci0tcmVwb3MnKSxcbiAgICAgICAgJHJlcG9zID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ3JlcG9zJyksXG5cdFx0JHNlYXJjaElucHV0RmllbGQgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnc2VhcmNoLS11c2VyJyksXG5cdFx0dXNlciA9ICRzZWFyY2hJbnB1dEZpZWxkLnZhbHVlO1xuXG5cdGNsZWFyRWxlbWVudHMoICRyZXBvcyApO1xuXG5cdEFqYXhDYWxsKCdodHRwczovL2FwaS5naXRodWIuY29tL3VzZXJzLycsIHVzZXIgKyAnL3JlcG9zJywgZnVuY3Rpb24ocmVzcG9uc2UsIHN0YXR1cykge1xuXHRcdHZhciBnZXRSZXNwb25zZSA9IEpTT04ucGFyc2UocmVzcG9uc2UpLFxuXHRcdFx0cmVwb3M7XG5cblx0XHRjb25zb2xlLmxvZyhnZXRSZXNwb25zZSk7XG5cblx0XHRpZiAoIHN0YXR1cyAhPT0gNDA0ICkge1xuXHRcdFx0Zm9yICggcmVwb3MgaW4gZ2V0UmVzcG9uc2UgKSB7XG5cdFx0XHRcdGlmIChnZXRSZXNwb25zZS5oYXNPd25Qcm9wZXJ0eShyZXBvcykpIHtcblx0XHRcdFx0XHQkcmVwb3MuaW5uZXJIVE1MICs9XG5cdFx0XHRcdFx0XHQnPGxpPicgK1xuXHRcdFx0XHRcdFx0XHQnPGRpdiBjbGFzcz1cInJlcG8tLWNvbnRhaW5lclwiPicgK1xuXHRcdFx0XHRcdFx0XHRcdCc8c3BhbiBjbGFzcz1cInJlcG8tLW5hbWVcIj48YSBocmVmPVwiJyArIGdldFJlc3BvbnNlW3JlcG9zXS5odG1sX3VybCArICdcIj4nICsgZ2V0UmVzcG9uc2VbcmVwb3NdLm5hbWUgKyAnPC9hPjwvc3Bhbj4nICtcblx0XHRcdFx0XHRcdFx0XHQnPHNwYW4gY2xhc3M9XCJyZXBvLS1pbmZvXCI+JyArXG5cdFx0XHRcdFx0XHRcdFx0XHQnPHNwYW4gY2xhc3M9XCJzdGFyLS1jb3VudFwiPicgK1xuXHRcdFx0XHRcdFx0XHRcdFx0XHQnPGkgY2xhc3M9XCJmYSBmYS1zdGFyXCIgYXJpYS1oaWRkZW49XCJ0cnVlXCI+PC9pPicgKyBnZXRSZXNwb25zZVtyZXBvc10uc3RhcmdhemVyc19jb3VudCArICc8L3NwYW4+JyArXG5cdFx0XHRcdFx0XHRcdFx0XHQnPHNwYW4gY2xhc3M9XCJmb3JrLS1jb3VudFwiPjxpIGNsYXNzPVwiZmEgZmEtY29kZS1mb3JrXCIgYXJpYS1oaWRkZW49XCJ0cnVlXCI+PC9pPicgKyBnZXRSZXNwb25zZVtyZXBvc10uZm9ya3NfY291bnQgKyAnPC9zcGFuPicgK1xuXHRcdFx0XHRcdFx0XHRcdCc8L3NwYW4+JyArXG5cdFx0XHRcdFx0XHRcdCc8L2Rpdj4nICtcblx0XHRcdFx0XHRcdCc8L2xpPic7XG5cdFx0XHRcdH1cblx0XHRcdH1cblxuXHRcdFx0JHVzZXJSZXBvcy5zdHlsZS5kaXNwbGF5ID0gXCJibG9ja1wiO1xuXHRcdH1cblx0XHRlbHNlIHtcblx0XHRcdCR1c2VyUmVwb3Muc3R5bGUuZGlzcGxheSA9IFwibm9uZVwiO1xuXHRcdH1cblxuXHR9KTtcbn07XG5cblxuJHNlYXJjaEJ0bi5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIHNob3dVc2VyUmVwb3MpO1xuIiwiXG5cbi8vICd1c2Ugc3RyaWN0Jztcbi8vXG4vL1xuLy8gZGVzY3JpYmUoJ3Rlc3QvLy8nLCBmdW5jdGlvbiAoKSB7XG4vLyAgICAgaXQoJ3Rlc3RpbiBtb2FyLi4nLCBmdW5jdGlvbiAoKSB7XG4vL1xuLy8gICAgICAgICBleHBlY3QodHJ1ZSkudG9CZSh0cnVlKTtcbi8vICAgICB9KTtcbi8vIH0pO1xuIl19
