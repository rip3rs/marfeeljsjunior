/**
 *  User repositories
 */

'use strict';


var AjaxCall = require('../lib/ajaxCall'),
	clearElements = require('../lib/clearElements'),
	$searchBtn = document.getElementById('search--btn');


var showUserRepos = function () {
    var $userRepos = document.getElementById('user--repos'),
        $repos = document.getElementById('repos'),
		$searchInputField = document.getElementById('search--user'),
		user = $searchInputField.value;

	clearElements( $repos );

	AjaxCall('https://api.github.com/users/', user + '/repos', function(response, status) {
		var getResponse = JSON.parse(response),
			repos;

		console.log(getResponse);

		if ( status !== 404 ) {
			for ( repos in getResponse ) {
				if (getResponse.hasOwnProperty(repos)) {
					$repos.innerHTML +=
						'<li>' +
							'<div class="repo--container">' +
								'<span class="repo--name"><a href="' + getResponse[repos].html_url + '">' + getResponse[repos].name + '</a></span>' +
								'<span class="repo--info">' +
									'<span class="star--count">' +
										'<i class="fa fa-star" aria-hidden="true"></i>' + getResponse[repos].stargazers_count + '</span>' +
									'<span class="fork--count"><i class="fa fa-code-fork" aria-hidden="true"></i>' + getResponse[repos].forks_count + '</span>' +
								'</span>' +
							'</div>' +
						'</li>';
				}
			}

			$userRepos.style.display = "block";
		}
		else {
			$userRepos.style.display = "none";
		}

	});
};


$searchBtn.addEventListener('click', showUserRepos);
