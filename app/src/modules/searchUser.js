/**
 *
 * Search Users suggestions.
 *
 */

'use strict';


var AjaxCall = require('../lib/ajaxCall'),
	Debounce = require('../lib/debounce'),
	Parameters = require('../lib/parameters'),
	clearElements = require('../lib/clearElements'),
	eventTarget = require('../lib/eventTarget'),
	$searchInputField = document.getElementById('search--user'),
	$suggestionsContainer = document.getElementById('suggestions--container'),
    $suggestions = document.getElementById('suggestions'),
	$searchBtn = document.getElementById('search--btn');


var removeSearchSuggestion = function () {
	$suggestionsContainer.style.display = "none";
};


var showSearchSuggestion = function () {
	$suggestionsContainer.style.display = "block";
};


var searchSuggestion = Debounce(function () {
	var limitAmountOfSearchUsers = 5,
		amountOfUsers = 1,
		getParams = '',
		params = {
			q: ''
		};

	//add values from input into object.
	params.q = $searchInputField.value;

	clearElements( $suggestions );

	//Wait until 3 letters are written before making a request.
	if ( params.q.length >= 3 && $suggestions.children.length === 0 ) {
		getParams = Parameters(params);

		AjaxCall('https://api.github.com/search/users', getParams, function( response, status ) {
			var getResponse = JSON.parse( response );

			if ( status !== 404 ) {
				if (getResponse.items.length === 0) {
					removeSearchSuggestion();
				} else {
					for ( var users in getResponse.items ) {
						if (getResponse.items.hasOwnProperty( users )) {
							$suggestions.innerHTML += '<li >' + getResponse.items[users].login + '</li>';

							amountOfUsers++;
						}

						if ( amountOfUsers >= limitAmountOfSearchUsers ) {
							break;
						}
					}

					showSearchSuggestion();
				}
			} else {
				$suggestions.innerHTML += '<li class="warning">There was a problem with your request!</li>';
				showSearchSuggestion();
			}
		});

	}
}, 500);


$suggestions.addEventListener( 'click', function ( event ) {
	var value = eventTarget( event );
	$searchInputField.value = value.innerText || value.textContent;
	$searchBtn.click();
	$searchInputField.blur();
});


$searchInputField.addEventListener( 'keyup', function ( event ) {
	if ( event.keyCode === 13 ) {
		$searchBtn.click();
		$searchInputField.blur();
	} else {
		searchSuggestion();
	}
});
