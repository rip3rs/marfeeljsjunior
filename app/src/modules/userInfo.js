/**
 *
 * Search Users info.
 *
 */

'use strict';


var AjaxCall = require('../lib/ajaxCall'),
	clearElements = require('../lib/clearElements'),
	$suggestionsContainer = document.getElementById('suggestions--container'),
	$searchBtn = document.getElementById('search--btn');


var removeSearchSuggestion = function () {
	$suggestionsContainer.style.display = "none";
};


var getUserAvatar = function ( response ) {
	return '<div id="user--avatar">' + '<img src="' + response.avatar_url + '"' + 'title=' + response.login + '/></div>';
};


var getUserName = function ( response ) {
	return '<h5 id="user--name">' + '<a href="' + response.url + '">' + '@' + response.login + '</a></h5>';
};


var getFullName = function ( response ) {
	return '<h2 id="full--name">' + response.name + '</h2>';
};


var getUserBio = function ( response ) {
	var userBio;

	if (response.bio === null) {
		userBio = 'No Bio Available';
	}
	else {
		userBio = response.bio;
	}

	return '<h5 id="user--bio">' + userBio + '</h5>';
};


var showUserInfo = function () {
	var $userInfo = document.getElementById('user--info'),
		$searchInputField = document.getElementById('search--user'),
		user = $searchInputField.value;


	removeSearchSuggestion();
	clearElements( $userInfo );

	AjaxCall( 'https://api.github.com/users/', user, function( response, status ) {
		var getResponse = JSON.parse( response );

		if ( status !== 404 ) {
			$userInfo.innerHTML += getUserAvatar(getResponse);
			$userInfo.innerHTML += '<div class="details">' +
			 						getUserName(getResponse) +
			 						getFullName(getResponse) +
			 						getUserBio(getResponse) +
									'</div>';
		} else {
			$userInfo.innerHTML += '<div class="warning">Does Not exist!</div>';
		}

	});
};


$searchBtn.addEventListener('click', showUserInfo);
