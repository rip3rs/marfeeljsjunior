/**
 * @param apiUrl
 * @param params
 * @param callback
 */

module.exports = function (apiUrl, params, callback) {

    'use strict';


    var obj = new XMLHttpRequest(),
        url = apiUrl + params;

    obj.overrideMimeType("application/json");
    obj.open('GET', url, true);
    obj.onreadystatechange = function () {

        if (obj.readyState === 4) {
            callback(obj.responseText, obj.status);
        }

    };


    obj.send(null);

};
