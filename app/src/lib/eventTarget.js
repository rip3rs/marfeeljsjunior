/**
 * Get the event target function.
 */


'use strict';


module.exports = function ( event ) {
	event = event || window.event;
	return event.target || event.srcElement;
};
