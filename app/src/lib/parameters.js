/**
 * Call parameters for object seperation on call.
 */


'use strict';

module.exports = function( params ){
  return "?" + Object
        .keys(params)
        .map(function(key){ return key+"="+params[key]; })
        .join("&");
};
