/**
 * Clear elements / reset calls
 */

'use strict';


module.exports = function (el) {
    if (el.children.length > 0) {
		el.innerHTML = "";
	}
};
