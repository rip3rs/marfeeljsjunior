module.exports = function(grunt) {

    grunt.initConfig({

        less: {
            development: {
                options: {
                    compress: false
                },
                files: { // destination file and source files
                    'app/build/css/style.css': 'app/src/less/all.less'
                }
            },
        },


        jshint: {
            options: {
                node: true,
                jasmine: true,
                browser: true
            },
            all: [
                'Gruntfile.js',
                'app/src/**/*.js',
                '!app/src/vendor/*.js'
            ]
        },


        jasmine: {
            tests: {
			src: [],
			options: {
				outfile: 'app/_SpecRunner.html',
				specs: 'app/build/tests/specs.js'
			}
		}
        },


        browserify: {
            development: {
                options: {
                    browserifyOptions: {
                        debug: true
                    }
                },
                src: 'app/src/**/**/*.js',
                dest: 'app/build/js/app.js'
            },


            specs: {
                src: ['app/src/tests/**/*.test.js'],
                dest: 'app/build/tests/specs.js',
                options: {
                    browserifyOptions: {
                        debug: true
                    }
                }
            }
        },


        watch: {
            styles: {
                files: ['app/**/*.less'], // which files to watch
                tasks: ['less:development'],
                options: {
                    nospawn: true
                }
            },

            lint: {
                files: ['app/**/*.js'], // which files to watch
                tasks: ['jshint'],
                options: {
                    nospawn: true,
                    browser: true
                }
            },

            browserify: {
                files: ['app/**/**/*.js'], // which files to watch
                tasks: ['browserify']
            }
        }

    });


    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-jasmine');
    grunt.loadNpmTasks('grunt-browserify');
    grunt.loadNpmTasks('grunt-contrib-watch');


    grunt.registerTask('default', ['less:development', 'jshint', 'browserify', 'watch']);
    grunt.registerTask('compile', ['less:development', 'browserify', 'watch']);
    grunt.registerTask('tests', ['jshint', 'browserify:specs', 'jasmine']);

};
